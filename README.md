# Contents #

* ```/example-voting-app```  
  A simple distributed application running across multiple Docker containers.

* ```/argo-cd```  
  Argo CD and Argo Rollouts example

* Presentation slides can be found at:  
  https://docs.google.com/presentation/d/1XBQC9WJtBq8Nd6hrODB4IkWY7PkkF0Zq69bFP7FleTg/edit?usp=sharing