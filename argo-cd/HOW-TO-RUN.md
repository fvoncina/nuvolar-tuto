# How To Run

## Install Argo CD

Follow the getting started guide at:  
https://argoproj.github.io/argo-cd/getting_started/

## Install Argo Rollouts

Follow the installation instructions at:  
https://argoproj.github.io/argo-rollouts/installation/

## Register application and repositories

Run:

```bash
kubectl apply -f ./argocd-demo.yml
```