# How To Run

## Run as Docker Containers 

If not installed, get Docker: https://docs.docker.com/get-docker/

Create front and back tier networks

```bash
docker network create front-tier
docker network create back-tier
```

Run Redis container as a daemon process, expose port 6379 and use back-tier network

```bash
docker run -d --rm -p 6379:6379 --network back-tier --name redis redis:alpine

docker logs -f redis
```

Make sure data folder for postgres in empty

```bash
rm -rf "$PWD/data"
```

Run PostgreSQL container as a daemon process, expose port 5432, map volume for folder ```/var/lib/postgresql/data``` to ```$PWD/data``` and use back-tier network

```bash
docker run -d --rm -p 5432:5432 --network back-tier --name db -v "$PWD/data":/var/lib/postgresql/data -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres postgres:9.4

docker logs -f db
```

Run app containers: ```vote```, ```result```, ```worker```

```bash
docker build -f ./vote/Dockerfile -t vote ./vote
docker run -d -p 5000:80 -v "$PWD/vote":/app --rm --name vote vote
docker network connect front-tier vote
docker network connect back-tier vote
docker logs -f vote

docker build -f ./result/Dockerfile -t result ./result
docker run -d -p 5001:80 -p 5858:5858 -v "$PWD/result":/app --rm --name result result
docker network connect front-tier result
docker network connect back-tier result
docker logs -f result

docker build -f ./worker/Dockerfile -t worker ./worker
docker run -d --rm --network back-tier --name worker worker
docker logs -f worker
```

## Run as docker-compose

See ```docker-compose.yml```

## Run in Kubernetes

If not installed via docker for mac/windows, install Kubernetes:

* https://kind.sigs.k8s.io/
* https://kubernetes.io/docs/tasks/tools/install-kubectl/

And deploy resources:

```bash
kubectl apply ./kubernetes.yaml

watch kubectl get all --all-namespaces
```

## Prometheus / Grafana

If not installer, get Helm:

```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 chmod 700 get_helm.sh ./get_helm.sh
```

Install Prometheus operator

```bash
kubectl create ns monitor
helm install prometheus-operator stable/prometheus-operator --namespace monitor
```

Get Grafana username and password

```bash
kubectl get secrets prometheus-operator-grafana -o 'go-template={{index .data "admin-user"}}' | base64 -d -
kubectl get secrets prometheus-operator-grafana -o 'go-template={{index .data "admin-password"}}' | base64 -d -
```

Forward ports to access Prometheis and Grafana

```bash
kubectl port-forward prometheus-prometheus-operator-prometheus-0 9090
kubectl port-forward service/prometheus-operator-grafana 9091:80
```

Expose voting app metrics

```bash
cat <<EOF | kubectl apply -f -
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  labels:
    app: voting-app-vote
    release: prometheus-operator
  name: voting-app-vote
  namespace: monitor
spec:
  endpoints:
  - path: /metrics
    port: vote-service
  jobLabel: apps
  namespaceSelector:
    matchNames:
    - voting-app
  selector:
    matchLabels:
      apps: vote
EOF
```

## Kubernetes Cheat Sheet

https://kubernetes.io/docs/reference/kubectl/cheatsheet/
